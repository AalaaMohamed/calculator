#include <stdio.h>
#include <stdlib.h>
#include "Debugging.h"
#define ADD(x,y)(x+y)
#define SUBTRACT(x,y)(x-y)
#define MULTIPLY(x,y)(x*y)
#define DIVIDE(x,y)((float)x/y)

int main()
{
#ifdef Debug_Mode
printf("Hi!\n");
#endif
    int x,y,z,sum=0;
    float result;
    printf("Please enter the two numbers to operate on:\n");
    scanf("%d",&x);
    scanf("%d",&y);
    printf("Please enter the operation number:\n 1.Addition\n 2.Subtraction\n 3.Multiplication\n 4.Division\n");
    scanf("%d",&z);
    switch (z)
    {
        case 1:
            result=ADD(x,y);
            break;
        case 2:
            result=SUBTRACT(x,y);
            break;
        case 3:
            result=MULTIPLY(x,y);
            break;
        case 4:
            result=DIVIDE(x,y);
            break;
    }
    fflush(stdin);
    printf("Result=%f\n",result);
    for (int i=1; i<=100; i++)
    {
        sum+=i;
    }
    printf("Sum of numbers from 1 to 100 = %d", sum);
    return 0;
}
